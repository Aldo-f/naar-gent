// settings
var url = "https://datatank.stad.gent/4/mobiliteit/parkeerautomatentoekomstig.kml",
    mymap = L.map('mapid').setView([51.051, 3.725], 13),
    maakInputs = true;

// global vars
var layers,
    customLayer,
    opties = Object(),
    optiesTariefzone = [],
    optiesBetaalmodus = [],
    overlays = L.layerGroup().addTo(mymap);


// custom markers
var itsMe = L.icon({
    iconUrl: 'public/img/marker-icon-black.png',
    shadowUrl: 'public/img/marker-shadow.png',

    iconSize: [25, 41], // size of the icon
    shadowSize: [41, 41], // size of the shadow
    // iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
    shadowAnchor: [12, 18], // the same for the shadow
    popupAnchor: [0, -14] // point from which the popup should open relative to the iconAnchor
});

mymap.locate({
    setView: true,
    maxZoom: 16
});

function onLocationFound(e) {
    var radius = Math.round(e.accuracy);
    L.marker(e.latlng, {
            icon: itsMe
        }).addTo(mymap)
        .bindPopup("U bevindt zich binnen " + radius + " meters van dit punt").openPopup();

    L.circle(e.latlng, radius).addTo(mymap);
}
mymap.on('locationfound', onLocationFound);


function onLocationError(e) {
    alert(e.message);
}
mymap.on('locationerror', onLocationError);

// create map
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets',
    accessToken: 'pk.eyJ1IjoicHJlZ2ludW5kIiwiYSI6ImNrMGdtdzh3OTA4bXkzbHFhbzlhOWYyZzEifQ.-Com4XWmOuz3j8RxITM6wA'
}).addTo(mymap);


// KML layer
var runLayer = omnivore.kml(url)
    .on('ready', (e) => {
        layers = e.target;
        showData()
    });


function showData() {
    let hidden = 0,
        visible = 0,
        list = [];

    if (maakInputs) {
        maakCheckboxes();
    }

    for (var i = 0; i < filters.length; i++) {
        if (filters[i].checked) list.push(filters[i].value);
    }
    console.log(list);

    // fix for "Oranje"
    if (list.indexOf("Oranje zone") !== -1) {
        list.push("Oranje")
    }

    // then remove any previously-displayed marker groups
    overlays.clearLayers();
    // create a new marker group      
    var clusterGroup = new L.MarkerClusterGroup().addTo(overlays);

    // mymap.fitBounds(runLayer.getBounds());
    // and add any markers that fit the filtered criteria to that group.
    layers.eachLayer(layer => {
        let automaat = layer.feature.properties

        if (list.indexOf(automaat["Betaalmodus"]) !== -1) {
            visible++
            clusterGroup.addLayer(layer)
            layer.bindPopup(maakPopup(automaat))
        } else {
            hidden++
        }
        if (list.indexOf(automaat["Parkeertariefzone"]) !== -1) {
            visible++
            clusterGroup.addLayer(layer)
            layer.bindPopup(maakPopup(automaat))
        } else {
            hidden++
        }
    })
    console.log(hidden + ' elements hidden');
    console.log(visible + ' elements visible');
}


function createGroups(group, automaat, key) {
    if (group.indexOf(automaat[key]) == -1) group.push(automaat[key]);
    return group
}

function maakPopup(automaat) {
    return (
        `<b>${automaat.GEXXXX}</b> <br/>
        <hr>
        Tariefzone:  ${automaat.Parkeertariefzone} <br/>
        Categorie: ${automaat.Categorie}<br/>
        Bewonerszone:  ${automaat.Bewonerszone}<br/>
        Betaalmodus:  ${automaat.Betaalmodus}<br/>
        LocatieOmschrijving:  ${automaat.LocatieOmschrijving}<br/>
        Status:    ${automaat.Status}<br/>
    `);
}

function maakOpties(opties) {
    console.log(opties);
    const entries = Object.entries(opties)
    // maak opties aan in #filters
    entries.forEach(entrie => {
        // zet laatste opties checked
        entries[entries.length - 1][0] == entrie[0] ? maakOptie(entrie[0], entrie[1], true) : maakOptie(entrie[0], entrie[1]);

        //maak hr als niet laatste is
        if (entries[entries.length - 1][0] != entrie[0]) maakLijn();
    });
    maakZichtbaar(document.getElementById("filters"));


}

function maakOptie(groep, opties, checked) {
    console.log(groep + ": ");
    console.log(opties);

    var filters = document.getElementById("filters");
    for (var i = 0; i < opties.length; i++) {
        // fix voor "Oranje"
        if (opties[i] != "Oranje") {
            var div = document.createElement("div");
            var checkBox = document.createElement("input");
            var label = document.createElement("label");
            checkBox.type = "checkbox";
            checkBox.classList.add("optie")
            checkBox.setAttribute('name', 'filters')
            checkBox.value = opties[i];
            checkBox.onclick = showData;
            if (checked) checkBox.setAttribute('checked', 'true');
            label.appendChild(checkBox);
            div.appendChild(label);
            filters.appendChild(div)
            label.appendChild(document.createTextNode(opties[i]));
        }
    }
}

function maakLijn() {
    let filters = document.getElementById("filters"),
        hr = document.createElement("hr");
    filters.appendChild(hr)
}

function maakZichtbaar(element) {
    element.classList.remove('hidden')
}

function maakCheckboxes() {
    layers.eachLayer(layer => {
        let automaat = layer.feature.properties
        opties.Tariefzone = createGroups(optiesTariefzone, automaat, "Parkeertariefzone");
        opties.Betaalmodus = createGroups(optiesBetaalmodus, automaat, "Betaalmodus");
    });
    maakOpties(opties)
    // lisener
    let inputs = document.getElementsByTagName("input");
    console.log(inputs);

    for (const input of inputs) {
        input.addEventListener('onchange', showData);

    }
    maakInputs = false;
}