// require
var gulp = require('gulp'),
    sass = require('gulp-sass'),
    uglyCss = require('gulp-cssnano'),
    uglyJs = require('gulp-uglify'),
    beautify = require('gulp-beautify'),
    rename = require("gulp-rename"),
    notify = require("gulp-notify");


// location
var myCss = new function () {
    this.src = [
            'node_modules/leaflet.markercluster/dist/MarkerCluster.Default.css',
            'node_modules/leaflet.markercluster/dist/MarkerCluster.css',
            'node_modules/bootstrap/dist/css/bootstrap.css',
            'resources/scss/style.scss'
        ],
        this.dest = './.temp/css/',
        this.min = 'public/css/'
}

var myJs = new function () {
    this.src = [
            'node_modules/leaflet.markercluster/dist/leaflet.markercluster.js',
            'node_modules/@mapbox/leaflet-omnivore/leaflet-omnivore.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.js',
            'resources/js/main.js'
        ],
        this.dest = './.temp/js',
        this.min = 'public/js'
}

const {
    series
} = require('gulp');

const {
    parallel
} = require('gulp');

// functions
function clean(cb) {
    cb();
}

function build(cb) {
    cb();
}

function watch(cb) {
    cb();
    gulp.watch(myCss.src, css);
    gulp.watch(myJs.src, javascript);
}

/* create files */
function javascript(cb) {
    cb();
    gulp.src(myJs.src)
        .pipe(beautify())
        .pipe(gulp.dest(myJs.dest))
        // .pipe(uglyJs())
        .pipe(gulp.dest(myJs.min));
}

function css(cb) {
    cb();
    gulp.src(myCss.src)
        .pipe(sass())
        .pipe(gulp.dest(myCss.dest))
        .pipe(uglyCss())
        .pipe(gulp.dest(myCss.min));
}

// export functions
exports.build = build;
exports.default = series(
    clean, build,
    parallel(
        css, javascript
    )
);
exports.watch = watch;